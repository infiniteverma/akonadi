# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2009.
# Giorgos Katsikatsos <giorgos.katsikatsos@gmail.com>, 2010.
# Stelios <sstavra@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: akonadi_knut_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-02 00:55+0000\n"
"PO-Revision-Date: 2010-01-18 20:14+0100\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: knutresource.cpp:60
#, kde-format
msgid "No data file selected."
msgstr "Δεν επιλέχθηκε αρχείο δεδομένων."

#: knutresource.cpp:78
#, kde-format
msgid "File '%1' loaded successfully."
msgstr "Το αρχείο '%1' φορτώθηκε με επιτυχία."

#: knutresource.cpp:105
#, kde-format
msgid "Select Data File"
msgstr "Επιλογή αρχείου δεδομένων"

#: knutresource.cpp:107
#, kde-format
msgctxt "Filedialog filter for Akonadi data file"
msgid "Akonadi Knut Data File"
msgstr "Αρχείο δεδομένων Knut του Akonadi"

#: knutresource.cpp:149 knutresource.cpp:169 knutresource.cpp:322
#, kde-format
msgid "No item found for remoteid %1"
msgstr "Δεν βρέθηκε αντικείμενο αναγνωριστικού απομακρυσμένης σύνδεσης %1"

#: knutresource.cpp:187
#, kde-format
msgid "Parent collection not found in DOM tree."
msgstr "Δεν βρέθηκε ιεραρχικά ανώτερη συλλογή στη δενδρική δομή DOM."

#: knutresource.cpp:195
#, kde-format
msgid "Unable to write collection."
msgstr "Αδυναμία εγγραφής συλλογής."

#: knutresource.cpp:207
#, kde-format
msgid "Modified collection not found in DOM tree."
msgstr "Δεν βρέθηκε τροποποιημένη συλλογή στη δενδρική δομή DOM."

#: knutresource.cpp:237
#, kde-format
msgid "Deleted collection not found in DOM tree."
msgstr "Δεν βρέθηκε διαγραμμένη συλλογή στη δενδρική δομή DOM."

#: knutresource.cpp:251 knutresource.cpp:309 knutresource.cpp:316
#, kde-format
msgid "Parent collection '%1' not found in DOM tree."
msgstr "Δεν βρέθηκε ιεραρχικά ανώτερη συλλογή '%1' στη δενδρική δομή DOM."

#: knutresource.cpp:259 knutresource.cpp:329
#, kde-format
msgid "Unable to write item."
msgstr "Αδυναμία εγγραφής αντικειμένου."

#: knutresource.cpp:273
#, kde-format
msgid "Modified item not found in DOM tree."
msgstr "Δεν βρέθηκε τροποποιημένο αντικείμενο στη δενδρική δομή DOM."

#: knutresource.cpp:288
#, kde-format
msgid "Deleted item not found in DOM tree."
msgstr "Δεν βρέθηκε διαγραμμένο αντικείμενο στη δενδρική δομή DOM."

#~ msgid "Path to the Knut data file."
#~ msgstr "Διαδρομή αρχείου δεδομένων Knut."

#~ msgid "Do not change the actual backend data."
#~ msgstr "Να μην αλλαχθούν τα δεδομένα του συστήματος υποστήριξης."
